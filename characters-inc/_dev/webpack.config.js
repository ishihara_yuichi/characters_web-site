// 出力は絶対pathで指定しなければいけない為、node.jsのpathモジュールを使用する
const path = require('path');
const outputPath = path.resolve(__dirname, 'dist');
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

const targetBrowser = [
	"last 2 versions"
]


module.exports = (env, argv) => {
	const isDev = argv.mode == "development"
	return {
		// モード値を production に設定すると最適化された状態で、
		// development に設定するとソースマップ有効でJSファイルが出力される
		mode: argv.mode,

		// メインとなるJavaScriptファイル（エントリーポイント）
		entry: [
			path.join(__dirname, "src/js/index.js"),
		],

		// ファイルの出力設定
		output: {
			//	出力ファイルのディレクトリ名
			path: `${outputPath}/assets`,
			// 出力ファイル名
			filename: './js/main.js'
		},
		devtool: isDev ? "inline-source-map" : "",

		// ローカル開発用環境を立ち上げる
		// 実行時にブラウザが自動的に localhost を開く
		devServer: {
			contentBase: outputPath,
			open: true
		},

		module: {
			rules: [
				{
					// 拡張子 .js の場合
					test: /\.js$/,
					use: [
						{
							// Babel を利用する
							loader: "babel-loader",
							// Babel のオプションを指定する
							options: {
								presets: [
									// プリセットを指定することで、ES2020 を ES5 に変換
									"@babel/preset-env"
								]
							}
						}
					],
					exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/,
				},
				{
					// 対象となるファイルの拡張子
					test: /\.scss/,
					// Sassファイルの読み込みとコンパイル
					use: [
						MiniCssExtractPlugin.loader,
						// CSSをバンドルするための機能
						{
							loader: 'css-loader',
							options:{
								url:false
							}
						},
						// PostCSSのための設定
						{
							loader: "postcss-loader",
							options: {
								// PostCSS側でもソースマップを有効にする
								sourceMap: true,
								plugins: [
									// Autoprefixerを有効化
									// ベンダープレフィックスを自動付与する
									require("autoprefixer")({
										overrideBrowserslist: targetBrowser
									})
								]
							}
						},
						// Sassをバンドルするための機能
						{
							loader: "sass-loader",
							options: {
								// ソースマップの利用有無
								sourceMap: true
							}
						}
					]
				}
			]
		},
		plugins: [
			// cssをjsにバンドルせずに別ファイルで書き出す用の設定
			new MiniCssExtractPlugin({
				filename: `/css/style.css`,
				ignoreOrder: true,
			})
		]
	}
};
