// 共通
import '../scss/style.scss';

// ヘッダーフッター
import '../scss/header.scss';
import '../scss/footer.scss';

// 編集画面用
import '../scss/editor.scss';

// フォーム
import '../scss/form.scss';

// 共通要素
import '../scss/icon.scss';
import '../scss/btn.scss';

// カスタムブロック
import '../scss/blocks/base.scss';
import '../scss/blocks/vision.scss';
import '../scss/blocks/mission.scss';
import '../scss/blocks/business.scss';
import '../scss/blocks/member.scss';
import '../scss/blocks/partnar.scss';
import '../scss/blocks/top.scss';


import Swiper from 'swiper';
import ScrollReveal from 'scrollreveal'
//

let swiper
window.onload = () => {
	setAnchorList()
	smoothAnchorLink()
	setSwiper()
	formDesign()
	setHeaderMenu()

	// 読み込み後のビジュアル表示
	const topEl = document.getElementById('top')
	const visionEl = document.getElementById('vision')
	setTimeout(()=>{
		topEl.classList.add('is-show')
		setTimeout(()=>{
	 		topEl.classList.add('is-hide')
			if(swiper){
				swiper.init()
			}
		}, 5000)
	}, 1500)
}




//////////////////////////////////////
// ヘッダーメニュー操作
//////////////////////////////////////
const setHeaderMenu = () => {
	// スクロール量に応じたヘッダーの見た目調整
	const headerEl = document.body.getElementsByTagName('header')[0]
	const height = window.innerHeight
	window.onscroll = () => {
		if(height < window.pageYOffset + 30){
			headerEl.classList.add('is-default')
		}else{
			headerEl.classList.remove('is-default')
		}
	}

	// ヘッダーメニュー内のアンカーリンクタップ時の処理
	const menuEl = headerEl.getElementsByClassName('menu')[0]
	const menuToggleCheckbox = document.getElementById('menu-toggle')
	const menuList = Array.prototype.slice.call(menuEl.getElementsByTagName('a'))
	const anchorList = menuList.filter(function(el){
		const string = el.getAttribute('href')
		const match = string.match(/#/) || {index:-1}
		return string && match.index >= 0
	})
	anchorList.forEach((el, i)=>{
		el.onclick = ()=>{
			menuToggleCheckbox.checked = false
			headerEl.classList.remove('is-menu-open')
			document.body.classList.remove('is-no-scroll')
		}
	})

	// メニューが開いているかどうかを監視して状態管理する
	menuToggleCheckbox.onchange = (e) => {
		if(e.target.checked){
			headerEl.classList.add('is-menu-open')
			document.body.classList.add('is-no-scroll')
		}else{
			headerEl.classList.remove('is-menu-open')
			document.body.classList.remove('is-no-scroll')
		}
	}
}




//////////////////////////////////////
// Swiperをセットする
/* Swiperを読み込み、swiper-container をすべて実行する */
//////////////////////////////////////
const setSwiper = () => {
	swiper = new Swiper('.swiper-container.vision',{
		loop: true,
		effect: 'fade',
		initialSlideIndex: 0,
		init: false,
		speed: 0,
		autoplay:{
			delay: 5000
		}
	})
}




//////////////////////////////////////
// スクロールをトリガーとしたアニメーションをセットする
//////////////////////////////////////
const setScrollReveal = () => {
}




//////////////////////////////////////
// アンカーリストを生成する
/*
   ブロックエディタで、「アンカーリンクを設定する」がオンになっているものだけで リストを生成する。
   ブロック「ベースフレーム」のみで設定できる。
*/
//////////////////////////////////////
const setAnchorList = () => {
	const wrapper = document.getElementsByClassName("anchor-list")
	if(!wrapper.length) return

	let html = wrapper[0]

	//blockクラスのエレメントをリスト化
	const blockList = Array.prototype.slice.call(document.getElementsByClassName('block'))

	//blockクラスの中で、アンカー設定があるもののみを抽出
	let anchorList = blockList.filter(function(el){
		const flag  = el.getAttribute('data-is-anchor')
		return flag == 'true'
	})

	//抽出したアンカー設定リストを元に、アンカーリンクを生成する
	anchorList.forEach(function(el, i){
		const id = el.getAttribute('id')
		const name = el.getAttribute('data-anchor-name')
		if(id && name){
			const tag = `<a href="#${id}" class="anchor-item">${name}<i class="icon is-arrow"></i></a>`
			html.insertAdjacentHTML('beforeend', tag)//左が一番上になるように追加していく
		}
	})
}




// 全てのアンカーリンクをスムーススクロールにする
const smoothAnchorLink = () => {
	var linkList = Array.prototype.slice.call(document.getElementsByTagName('a'))
	var anchorList = linkList.filter(function(el){
		var string  = el.getAttribute('href')
		var pattern = '#'
		return string && string.indexOf(pattern) === 0
	})

	anchorList.forEach(function(el, i){
		el.onclick = function(){
			var targetId = this.getAttribute('href').slice(1)
			var targetEl = document.getElementById(targetId)

			if(!targetEl) return false

			// 画面上部から要素までの距離
			const rectTop = targetEl.getBoundingClientRect().top
			// 現在のスクロール距離
			const offsetTop = window.pageYOffset
			// スクロール位置に持たせるバッファ
			const buffer = 0
			const top = rectTop + offsetTop - buffer

			window.scrollTo({
				top,
				behavior: 'smooth'
			})
			return false
		}
	});
}






//////////////////////////////////////
// フォームにデザインを追加する
/*
   contactform7が、<span><input></span>で生成される。
   デザイン上、inputとlabelが隣接している必要があるため、
   隣接させるようにinput情報からlabelをinputの直後に生成する。
*/
//////////////////////////////////////
const formDesign = () => {
	const CONTACTFORM7_WRAP_CLASSNAME = 'wpcf7-form-control-wrap'
	const CONTACTFORM7_RESPONSE_CLASSNAME = 'wpcf7-response-output'//送信されたかどうかの監視対象
	const wrapList = document.getElementsByClassName(CONTACTFORM7_WRAP_CLASSNAME)

	// inputとtextareaにlabelを追加する
	if(wrapList.length){
		for(let i=0; i<wrapList.length; i++){
			const field = wrapList[i].getElementsByTagName('input')[0] || wrapList[i].getElementsByTagName('textarea')[0]
			const id = field.getAttribute('id')
			const name = field.getAttribute('placeholder')
			field.setAttribute('data-has-value', false)
			field.removeAttribute('placeholder')
			wrapList[i].insertAdjacentHTML('beforeend', `<label for="${id}">${name}</label>`)//終了タグ前に追加

			// 入力されているかどうかをデータ属性に設定する
			field.addEventListener('change', (event) => {
				let target = event.target
				if(target.value.length){
					target.setAttribute('data-has-value', true)
				}else{
					target.setAttribute('data-has-value', false)
				}
			});
		}


		//監視する要素の指定
		const response = document.getElementsByClassName(CONTACTFORM7_RESPONSE_CLASSNAME)[0]
		if(response){
			//MutationObserver（インスタンス）の作成
			let mo = new MutationObserver(function(record, observer) {
				/* 変更検出時に実行する内容 */
				for(let j=0; j<wrapList.length; j++){
					const field = wrapList[j].getElementsByTagName('input')[0] || wrapList[j].getElementsByTagName('textarea')[0]
					if(field.value.length){
						field.setAttribute('data-has-value', true)
					}else{
						field.setAttribute('data-has-value', false)
					}
				}
			});
			//監視する「もの」の指定（必ず1つ以上trueにする）
			const config = {
				childList: true,//「子ノード（テキストノードも含む）」の変化
				attributes: true,//「属性」の変化
				characterData: true//「テキストノード」の変化
			}
			//監視の開始
			mo.observe(response, config);
		}
	}
}
