	<footer>
		<nav class="footer-menu">
			<a href="#" class="menu-item">Privacy Policy</a>
			<small class="menu-item copyright"><span class="mark">©</span>Characters Inc.</small>
		</nav>
		<a href="#vision" class="page-top"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/img_page-top.png"></a>
	</footer>

	<?php wp_footer(); ?>

</body>
</html>
