<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<title><?php bloginfo('name'); ?> | <?php wp_title(); ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0">


		<?php if(get_theme_mod(GOOGLE_ANALYTICS_TRACKING_ID)): ?>
			<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo get_theme_mod(GOOGLE_ANALYTICS_TRACKING_ID); ?>"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());
				gtag('config', '<?php echo get_theme_mod(GOOGLE_ANALYTICS_TRACKING_ID); ?>');
			</script>
		<?php endif; ?>


		<?php wp_head(); // ※</head>直前にwp_head();を記述する ?>
	</head>
	<body>
		<?php if( is_front_page() or is_home() ): ?>
			<section id="top">
				<div class="bg"></div>
				<div class="top-main">
					<p class="catch">多種多様な個性を創造する</p>
					<h2 class="company-name">Characters Inc.</h2>
				</div>
			</section>
		<?php endif; ?>
		<header>
			<input type="checkbox" id="menu-toggle" /><label for="menu-toggle"><i class="icon is-menu is-light"></i><i class="icon is-cross"></i></label>
			<h1><a href="#top">Characters Inc.</a></h1>
			<nav class="menu">
				<ul id="menu-header-global" class="global-menu">
					<li class="menu-item">
						<a href="/">WHAT WE ARE?</a>
						<ul class="child-menu">
							<li class="menu-item"><a href="/#vision">VISION</a></li>
							<li class="menu-item"><a href="/#mission">MISSION</a></li>
							<li class="menu-item"><a href="/#business">BUSINESS</a></li>
							<li class="menu-item"><a href="/#member">MEMBER</a></li>
							<li class="menu-item"><a href="/#partnar">PARTNAR</a></li>
							<li class="menu-item"><a href="/#company">COMPANY</a></li>
						</ul>
					</li>
					<li class="menu-item">
						<a href="/">WORKS</a>
						<ul class="child-menu">
							<li class="menu-item"><a href="/">Villains</a></li>
							<li class="menu-item"><a href="/">18番街</a></li>
							<li class="menu-item"><a href="/">青と赤の軌跡</a></li>
							<li class="menu-item"><a href="/">宇宙人狙撃部</a></li>
						</ul>
					</li>
					<li class="menu-item">
						<a href="/">CREATERS</a>
						<ul class="child-menu">
							<li class="menu-item"><a href="/">監督</a></li>
							<li class="menu-item"><a href="/">脚本家</a></li>
							<li class="menu-item"><a href="/">演出家</a></li>
							<li class="menu-item"><a href="/">アートディレクター</a></li>
						</ul>
					</li>
					<li class="menu-item">
						<a href="/#contact">CONTACT US</a>
						<ul class="child-menu">
							<li class="menu-item"><a href="/">監督</a></li>
						</ul>
					</li>
				</ul>
			</nav>
		</header>
